﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class Department
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}
