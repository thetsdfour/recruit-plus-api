﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class ApplicationAnswer
    {
        public int Id { get; set; }
        public int AppId { get; set; }
        public int QuestionId { get; set; }
        public int Answer { get; set; }
        public string Comment { get; set; }
    }
}
