﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class InterviewGroupRequest
    {
        public int id { get; set; }
        public int userId { get; set; }
    }
    public class InterviewGroupCreateRequest
    {
        public string name { get; set; }
        public int userId { get; set; }
    }

    public class InterviewGroup
    {
        public int id { get; set; }
        public string name { get; set; }
        public int total_members { get; set; }
    }

    public class InterviewGroupUser
    {
        public int userId { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string profile_img_url { get; set; }
        public string email { get; set; }
    }
}
