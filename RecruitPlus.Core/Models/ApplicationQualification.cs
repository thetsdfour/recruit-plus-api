﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class ApplicationQualification
    {
        public int Id { get; set; }
        public string AppId { get; set; }
        public string QualificationName { get; set; }
        public string Institute { get; set; }
        public string Description { get; set; }
        public int QualificationYear { get; set; }
        public int QualificationType { get; set; }
        public bool IsCurrent { get; set; }
    }
}
