﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class CandidateProfile
    {
        public string full_name { get; set; }
        public string last_designation { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string last_employer { get; set; }
        public string address { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string facebook_id { get; set; }
        public string twitter_id { get; set; }
        public string github_id { get; set; }
        public string other_link { get; set; }
    }
}
