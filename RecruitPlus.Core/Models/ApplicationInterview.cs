﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class ApplicationInterview
    {
        public int Id { get; set; }
        public int AppId { get; set; }
        public int StepId { get; set; }
        public DateTime CalDate { get; set; }
        public DateTime CalTime { get; set; }
        public string MeetingLink { get; set; }
        public bool IsInviteSent { get; set; }
        public int Status { get; set; }
        public decimal InterviewMarks { get; set; }
        public DateTime InterviewTime { get; set; }
        public Guid MeetingId { get; set; }
    }
}
