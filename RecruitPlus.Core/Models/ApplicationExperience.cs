﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class ApplicationExperience
    {
        public int Id { get; set; }
        public int AppId { get; set; }
        public string ExperienceTitle { get; set; }
        public string Company { get; set; }
        public string Description { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool IsCurrent { get; set; }
    }
}
