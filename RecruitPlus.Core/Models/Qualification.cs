﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class Qualification
    {
        public string name { get; set; } // Eg: Masters in Information Technology
        public string institute { get; set; } // Cardiff Metropolitan University
        public int start_year { get; set; }
        public int end_year { get; set; }
        public int status { get; set; } // 0 - Pending, 1-completed

    }
}
