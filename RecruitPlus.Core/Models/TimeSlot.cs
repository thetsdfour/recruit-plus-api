﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class TimeSlot
    {
        public int id { get; set; }
        public string name { get; set; }
        public double start_time { get; set; }
        public double end_time { get; set; }
    }

    public class DailyTimeSlot
    {
        public DateTime date { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public double start_time { get; set; }
        public double end_time { get; set; }
    }

}
