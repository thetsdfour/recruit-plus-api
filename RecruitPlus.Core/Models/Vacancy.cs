﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class Vacancy
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SmallDescription { get; set; }
        public int DepartmentId { get; set; }
        public string JobDescription { get; set; }
        public string Duties { get; set; }
        public string Qualifications { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime SLStartDate { get; set; }
        public DateTime DeactivationDate { get; set; }
        public int SlotCount { get; set; }
        public int MaxApplicationCount { get; set; }
        public string Location { get; set; }
        public string CompanyName { get; set; }
        public string Salary { get; set; }
        public string Type { get; set; }
        public bool IsRemote { get; set; }
        public int VacancyStatus { get; set; }
    }
}
