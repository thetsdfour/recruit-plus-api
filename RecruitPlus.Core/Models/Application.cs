﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class Application
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string LinkedInId { get; set; }
        public string FacebookId { get; set; }
        public string GithubId { get; set; }
        public string OtherlinkId { get; set; }
        public int AppStatus { get; set; }
        public bool IsDeleted { get; set; }
        public string HMMessage { get; set; }
        public bool IsAcknowledged { get; set; }
        public int VacancyId { get; set; }
        public IList<ApplicationQualification> ApplicationQualifications { get; set; }
        public IList<ApplicationExperience> ApplicationExperiences { get; set; }

    }
}
