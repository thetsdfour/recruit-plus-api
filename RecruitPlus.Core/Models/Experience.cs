﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class Experience
    {
        public string designation { get; set; } 
        public string institute { get; set; } 
        public int start_year { get; set; }
        public int end_year { get; set; }
        public int status { get; set; } // 0 - Pending, 1-completed
    }
}
