﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class AuthenticateRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class SystemUser
    {
        public int id { get; set;}
        public int user_role { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string profile_img_url { get; set; }
        public string auth_token { get; set; }

    }
}
