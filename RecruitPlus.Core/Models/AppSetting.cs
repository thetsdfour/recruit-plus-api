﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{
    public class AppSetting
    {
        public string parameter { get; set; }
        public string value { get; set; }
    }
}
