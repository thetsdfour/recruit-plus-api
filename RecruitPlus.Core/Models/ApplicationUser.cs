﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.Models
{

    /*
     *  These classes are for examples only. Not for the use in Recruit Plus 
     * 
     */

    public class ApplicationUser
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string title { get; set; }
        public string companyName { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int gender { get; set; }
        public string country { get; set; }
        public byte[] profileImage { get; set; }
        public string profileImageMime { get; set; }
        public string ImageKey { get; set; }
    }

    public class ProfileImage
    {
        public byte[] data { get; set; }
        public string mime { get; set; }
    }

    public class IdentityManagerClient
    {
        public string Name { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string HostName { get; set; }
        public short ClientEnabled { get; set; }
        public string SignoutUrl { get; set; }
        public string LoginReplyUrl { get; set; }
        public string LoginReplyParams { get; set; }
        public string RegisterReplyUrl { get; set; }
        public string RegisterReplyParams { get; set; }
        public string SignoutReplyUrl { get; set; }
        public string SignoutReplyParams { get; set; }
    }

    public class IdentityManagerAuthentication
    {
        public IdentityManagerClient Client { get; set; }
        public string RequestId { get; set; }
    }
}
