﻿using RecruitPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.ViewModels
{
    public class ApplicationFull
    {
        public Application Application { get; set; }
        public List<ApplicationAnswer> ApplicationAnswers { get; set; }
    }
}
