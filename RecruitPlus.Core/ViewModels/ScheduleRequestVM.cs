﻿using RecruitPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.ViewModels
{
    public class ScheduleRequestVM
    {
        public DateTime schedule_date { get; set; }
        public int user_id { get; set; }
        public List<Schedule> slots { get; set; }
    }

    public class MonthlyScheduleRequest
    {
        public int year { get; set; }
        public int month { get; set; }
        public int user_id { get; set; }

    }
}
