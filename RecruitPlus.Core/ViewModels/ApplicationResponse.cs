﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.ViewModels
{
    public class ApplicationResponse
    {
        public ApplicationResponse()
        {
            this.status = true;
            this.message = "OK";
        }

        public ApplicationResponse(object obj)
        {
            this.status = true;
            this.message = "OK";
            this.data = obj;
        }

        public ApplicationResponse(object obj, string message, bool status)
        {
            this.status = status;
            this.message = message;
            this.data = obj;
        }

        public ApplicationResponse(string message, bool status)
        {
            this.status = status;
            this.message = message;
        }

        public bool status { get; set; }
        public object data { get; set; }
        public string message { get; set; }
    }
}
