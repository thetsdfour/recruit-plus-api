﻿using RecruitPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core.ViewModels
{
    public class ApplicationVM
    {
        public CandidateProfile profile { get; set; }
        public DateTime applied_date { get; set; }
        public List<Qualification> qualifications { get; set; }

    }
}
