﻿using Microsoft.Extensions.Configuration;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class ScheduleService
    {
        private IConfiguration _configuration;
        public ScheduleService(IConfiguration configuration) : base(configuration, "Schedule")
        {
            _configuration = configuration;
        }

        public ApplicationResponse MarkEmployeeAvailability(ScheduleRequestVM request)
        {
            // Validate if an already scheduled timeslot is marked for deletion
            foreach (var item in request.slots)
            {
                if (!item.isAvailable)
                {
                    if (IsHasAScheduledInterview(item.timeslot_id, request.user_id, request.schedule_date))
                    {
                        return new ApplicationResponse(String.Format("Interview slot {0} has already scheduled.", item.timeslot_id), false);
                    }
                }
            }

            // Delete Availabilities
            DeleteAllEmployeeAvailabilitySlots(request);

            // Insert Availabilities
            foreach (var item in request.slots)
            {
                if (item.isAvailable)
                {
                    SaveEmployeeAvailabilitySlot(item.timeslot_id, request.user_id, request.schedule_date);
                }                
            }

            return new ApplicationResponse();
        }
    }
}
