﻿using Microsoft.Extensions.Configuration;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class HRPortalService
    {
        private IConfiguration _configuration;
        public HRPortalService(IConfiguration configuration) : base(configuration, "HRPortal")
        {
            _configuration = configuration;
        }
        public ApplicationResponse AddMemberToInterviewGroupById(InterviewGroupRequest request)
        {
            if (SaveMemberToInterviewGroupById(request))
            {
                return new ApplicationResponse();
            }
            else
            {
                return new ApplicationResponse("Invalid Request.", false);
            }
        }

        public ApplicationResponse RemoveMemberFromInterviewGroupById(InterviewGroupRequest request)
        {
            if (DeleteMemberFromInterviewGroupById(request))
            {
                return new ApplicationResponse();
            }
            else
            {
                return new ApplicationResponse("Invalid Request.", false);
            }
        }

    }
}
