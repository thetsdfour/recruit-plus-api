﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class MasterdataService
    {
        private IConfiguration _configuration;
        public MasterdataService(IConfiguration configuration) : base(configuration, "Masterdata")
        {
            _configuration = configuration;
        }

        
    }
}
