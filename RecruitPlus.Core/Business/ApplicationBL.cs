﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class ApplicationService
    {
        private IConfiguration _configuration;
        public ApplicationService(IConfiguration configuration) : base(configuration, "Application")
        {
            _configuration = configuration;
        }
    }
}
