﻿using Microsoft.Extensions.Configuration;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class AuthenticationService
    {
        private IConfiguration _configuration;
        public AuthenticationService(IConfiguration configuration) : base(configuration, "Auth")
        {
            _configuration = configuration;
        }

        public ApplicationResponse AuthenicateUser(AuthenticateRequest request)
        {
            SystemUser user = UserAccountExists(request.username, request.password);
            if (user == null)
            {
                
                return new ApplicationResponse("Invalid username or password",false);
            }
            else
            {
                user.auth_token = Guid.NewGuid().ToString();
                return new ApplicationResponse(user);
            }
            
        }
    }
}
