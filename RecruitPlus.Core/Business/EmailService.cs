﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace RecruitPlus.Core.Business
{
    public class EmailService
    {
        public bool SendEmail(string from, string to, string subject, string body, DateTime interviewTime, int timeOffset = 0)
        {
            try
            {
                MailMessage mail = new MailMessage(from, to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = false;
                mail.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.UseDefaultCredentials = true;
                NetworkCredential networkCredential = new NetworkCredential(from, "V5_234_Dev22");
                client.UseDefaultCredentials = true;
                client.Credentials = networkCredential;
                client.Port = 587;

                string CalendarContent = MeetingRequestString(from, to, subject, body, "location test", interviewTime, interviewTime, timeOffset);
                System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
                contype.Parameters.Add("method", "REQUEST");
                contype.Parameters.Add("name", "invite.ics");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(CalendarContent, contype);
                mail.AlternateViews.Add(avCal);

                client.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string MeetingRequestString(string from, string to, string subject, string desc, string location, DateTime startTime, DateTime endTime, int timeOffset, int? eventID = null, bool isCancel = false) 
        {
            StringBuilder str = new StringBuilder();

            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN");
            str.AppendLine("VERSION:2.0");
            str.AppendLine(string.Format("METHOD:REQUEST"));
            str.AppendLine("BEGIN:VEVENT");

            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startTime));
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.Now));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", startTime.AddHours(2)));
            str.AppendLine(string.Format("UID:{0}", Guid.NewGuid().ToString()));
            str.AppendLine(string.Format("DESCRIPTION:{0}", desc.Replace("\n", "<br>")));
            str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", desc.Replace("\n", "<br>")));
            str.AppendLine(string.Format("SUMMARY:{0}", subject));

            str.AppendLine(string.Format("ORGANIZER;CN=\"{0}\":MAILTO:{1}", from, from));
            str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", string.Join(",", to), string.Join(",", to)));

            str.AppendLine("BEGIN:VALARM");
            str.AppendLine("TRIGGER:-PT15M");
            str.AppendLine("ACTION:DISPLAY");
            str.AppendLine("DESCRIPTION:Reminder");
            str.AppendLine("END:VALARM");
            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");

            return str.ToString();
        }
    }
}
