﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RecruitPlus.Core.Business;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RecruitPlus.Core
{
    public partial class CandidatePortalService
    {
        private IConfiguration _configuration;
        public string roomName = "";
        private EmailService _emailService; 

        public CandidatePortalService(IConfiguration configuration) : base(configuration, "CandidatePortal")
        {
            _configuration = configuration;
            _emailService = new EmailService();
        }

        public List<ApplicationUser> LoadAllUsersForImport()
        {
            return GetAllUsersForImport();
        }

        public List<Vacancy> GetAllVacancy(string keyword, string filter, bool IsRemote, int departmentId, int? clientId) 
        {
            return GetAllVacancyDA(keyword, filter, IsRemote, departmentId, clientId);
        }

        public Vacancy GetVacancy(int vacancyId) 
        {
            return GetVacancyDA(vacancyId);
        }

        public void ApplyForVacancy(ApplicationFull applicationRequest)
        {
            InsertApplicationDA(applicationRequest);
        }

        public async Task SaveFile(Attachment attachment) 
        {
             await InsertAttachmentDA(attachment);
        }

        private async Task UploadFileAsync(string keyName, Stream inputStream) 
        {
          //upload to google cloud storage implementation
        }

        public List<int> GetAvailableTimeSlots() 
        {
            return GetAvailableTimeSlotsDA();
        }

        public Application GetApplicationById(int applicationId) 
        {
            return GetApplicationByIdDA(applicationId);
        }

        public string GetInterviewRemainingTime(int interviewId) 
        { 
            var interviewTime = GetInterviewRemainingTimeDA(interviewId);
            var currentTime = DateTime.Now;

            var result = "";

            if ((int)Math.Abs(Math.Round((interviewTime - currentTime).TotalDays)) > 0)
            {
                result = interviewTime.ToString();
            }
            else if((int)Math.Abs(Math.Round((interviewTime - currentTime).TotalDays)) == 0) 
            { 
                result = interviewTime.Subtract(currentTime).TotalSeconds.ToString();
            }
            return result;
        }

        public async Task<bool> CreateInterviewLink(int interviewId) 
        {
            var interview = GetInterviewDA(interviewId);

            HttpClient client = new HttpClient();
            var baseUrl = _configuration.GetSection("DailyCoBaseUrl").Value;
            var apiKey = _configuration.GetSection("DailyCoApiKey").Value;
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
            HttpContent requestBody = new StringContent(this.serializeDateToJson(interview.InterviewTime, this.interviewNameGenerator(), "public"), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("/v1/rooms/", requestBody);

            var responseBody = await response.Content.ReadAsStringAsync();
            var dataReponse = JsonConvert.DeserializeObject<Interview>(responseBody);

            var candidate = this.GetApplicationById(interview.AppId);
            var fromAddress = _configuration.GetSection("EmailFrom").Value;
            var applicantMessageBody = "Hi " + candidate.FirstName + "," + "<br/>" + "<br/>" + "Interview is scheduled on" + " " + interview.InterviewTime.ToString("dd MMMM yyyy") + 
                " at" + " " + interview.InterviewTime.ToString("HH:mm") + "<br/>" + 
                " You must accept the invitation so that we can see that you will attend the interview. Click on the link to join the meeting: " + 
                "<a href=" + dataReponse.url + ">" + dataReponse.url + "</a>";

            var subjectLine = "Invitation for Interview";

            this._emailService.SendEmail(fromAddress, "sasiru.tharinda@gmail.com", subjectLine, applicantMessageBody, interview.InterviewTime);

            UpdateInterviewDA(interviewId, dataReponse.url, dataReponse.id);
            return true;
        }

        public string interviewNameGenerator()
        {
            string interviewNameInitial = "recruitplus_";
            Random random = new Random();
            var generatedRoomName = interviewNameInitial + random.Next(9999999).ToString();
            this.roomName = generatedRoomName;
            return generatedRoomName;
        }

        public string serializeDateToJson(DateTime interviewSetDate, string interviewName, string privacySetting) 
        {
            var timeSpan = (interviewSetDate - new DateTime(1970, 1, 1, 0, 0, 0));
            var nbfSeconds = (long)timeSpan.TotalSeconds;
            var expSeconds = nbfSeconds + 86400;

            var validityPeriod = new 
            {
                name = interviewName,
                privacy = privacySetting,
                properties = new
                {
                    nbf = nbfSeconds,
                    exp = expSeconds
                }
            };

            string jsonResult = JsonConvert.SerializeObject(validityPeriod);
            return jsonResult;
        }
    }
}
