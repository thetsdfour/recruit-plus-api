﻿using Microsoft.Data.SqlClient;
using RecruitPlus.Core.DataAccess;
using RecruitPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class AuthenticationService : BaseDataAccess
    {
        public SystemUser UserAccountExists(string username, string password)
        {
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_User_Account_Details"), connection);
                command.Parameters.Add("UserEmail", SqlDbType.VarChar, 100).Value = username;

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        if (reader["Password"].ToString().Equals(password))
                        {
                            SystemUser user = new SystemUser();
                            user.id = Convert.ToInt32(reader["Id"].ToString());
                            user.user_role = Convert.ToInt32(reader["UserRole"].ToString());
                            user.first_name = reader["Firstname"].ToString();
                            user.last_name = reader["Lastname"].ToString();
                            user.profile_img_url = reader["ProfileImageUrl"].ToString();
                            return user;
                        }  
                    }
                }
                finally
                {
                    reader.Close();
                }
                return null;
            }

        }
    }
}
