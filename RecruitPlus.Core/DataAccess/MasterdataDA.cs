﻿using Microsoft.Data.SqlClient;
using RecruitPlus.Core.DataAccess;
using RecruitPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class MasterdataService : BaseDataAccess
    {
        public IEnumerable<Department> GetAllDepartments()
        {
            List<Department> departments = new List<Department>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_Departments"), connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        Department ig = new Department();
                        ig.id = Convert.ToInt32(reader["Id"].ToString());
                        ig.name = reader["DepartmentName"].ToString();
                        ig.description = reader["Description"].ToString();
                        departments.Add(ig);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return departments;
        }

        public IEnumerable<AppSetting> GetAppAppsettings()
        {
            List<AppSetting> departments = new List<AppSetting>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_App_Settings"), connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        AppSetting ig = new AppSetting();
                        ig.parameter = reader["SETTING_ID"].ToString();
                        ig.value  = reader["SETTING_VALUE"].ToString();
                        departments.Add(ig);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return departments;
        }

        public IEnumerable<TimeSlot> GetAllTimeSlots()
        {
            List<TimeSlot> departments = new List<TimeSlot>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_Timeslots"), connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        TimeSlot ig = new TimeSlot();
                        ig.id = Convert.ToInt32(reader["Id"].ToString());
                        ig.name = reader["TimeSlotDescription"].ToString();
                        ig.start_time  = Convert.ToDouble( reader["TimeStart"].ToString());
                        ig.end_time = Convert.ToDouble(reader["TimeEnd"].ToString());
                        departments.Add(ig);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return departments;
        }
    }
}
