﻿using Microsoft.Data.SqlClient;
using RecruitPlus.Core.DataAccess;
using RecruitPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class HRPortalService : BaseDataAccess
    {
        public IEnumerable<InterviewGroup> GetAllInterviewGroups()
        {
            List<InterviewGroup> interviewGroups = new List<InterviewGroup>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_Interview_Groups"), connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        InterviewGroup ig = new InterviewGroup();
                        ig.id = Convert.ToInt32(reader["Id"].ToString());
                        ig.total_members  = Convert.ToInt32(reader["EMP_COUNT"].ToString());
                        ig.name = reader["IntGroupName"].ToString();
                        interviewGroups.Add(ig);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return interviewGroups;
        }

        public IEnumerable<InterviewGroupUser> GetInterviewGroupMembersById(int interviewGroupId)
        {
            List<InterviewGroupUser> interviewGroups = new List<InterviewGroupUser>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_IG_Members_By_Id"), connection);
                command.Parameters.Add("IG_ID", SqlDbType.Int, 100).Value = interviewGroupId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        InterviewGroupUser ig = new InterviewGroupUser();
                        ig.userId = Convert.ToInt32(reader["UserId"].ToString());
                        ig.first_name = reader["Firstname"].ToString();
                        ig.last_name = reader["Lastname"].ToString();
                        ig.email = reader["UserEmail"].ToString();
                        ig.profile_img_url  = reader["ProfileImageUrl"].ToString();
                        interviewGroups.Add(ig);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return interviewGroups;
        }

        public bool SaveMemberToInterviewGroupById(InterviewGroupRequest request)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Save_IG_Member")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("IntGroupId", SqlDbType.Int, 100).Value = request.id;
                    querySaveStaff.Parameters.Add("UserId", SqlDbType.Int, 100).Value = request.userId;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool DeleteMemberFromInterviewGroupById(InterviewGroupRequest request)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Delete_IG_Member")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("IntGroupId", SqlDbType.Int, 100).Value = request.id;
                    querySaveStaff.Parameters.Add("UserId", SqlDbType.Int, 100).Value = request.userId;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool InsertInterviewGroup(InterviewGroupCreateRequest request)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Create_Interview_Group")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("IntGroupName", SqlDbType.VarChar, 100).Value = request.name;
                    querySaveStaff.Parameters.Add("CreatedBy", SqlDbType.Int, 100).Value = request.userId;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }
    }
}
