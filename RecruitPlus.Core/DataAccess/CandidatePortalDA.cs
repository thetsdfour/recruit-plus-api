﻿using Microsoft.Data.SqlClient;
using RecruitPlus.Core.DataAccess;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace RecruitPlus.Core
{
    public partial class CandidatePortalService : BaseDataAccess
    {
        public List<ApplicationUser> GetAllUsersForImport()
        {
            List<ApplicationUser> appUsers = new List<ApplicationUser>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_UserAccounts_For_Import"), connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        ApplicationUser user = new ApplicationUser();
                        user.firstName = reader["username"].ToString();
                        user.lastName = reader["password"].ToString();
                        appUsers.Add(user);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return appUsers;
        }

        public bool UpdateImportStatus(string Id, string Status)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Update_UserAccounts_Import_Status")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("ID", SqlDbType.VarChar, 100).Value = Id;
                    querySaveStaff.Parameters.Add("PROC_MSG", SqlDbType.VarChar, 100).Value = Status;

                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }


        public IdentityManagerClient GetClientInfo(string clientId, string authenticationCode)
        {
            IdentityManagerClient client = new IdentityManagerClient();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Client_Information_By_AuthToken"), connection);
                command.Parameters.Add("AUTH_TOKEN", SqlDbType.VarChar, 500).Value = authenticationCode;
                command.Parameters.Add("CLIENT_ID", SqlDbType.VarChar, 500).Value = clientId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        client.Name = reader["CLIENT_NAME"].ToString();
                        client.ClientId = reader["CLIENT_ID"].ToString();
                        client.ClientSecret = reader["CLIENT_SECRET"].ToString();
                        client.HostName = reader["HOSTNAME"].ToString();
                        client.ClientEnabled = Convert.ToInt16(reader["CLIENT_ENABLED"].ToString());
                        client.SignoutUrl = reader["SIGNOUT_URL"].ToString();

                        client.LoginReplyParams = reader["LOGIN_REPLY_PARAMS"].ToString();
                        client.LoginReplyUrl = reader["LOGIN_REPLY_URL"].ToString();
                        client.RegisterReplyParams = reader["REGISTER_REPLY_PARAMS"].ToString();
                        client.RegisterReplyUrl = reader["REGISTER_REPLY_URL"].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return client;
        }

        public IdentityManagerAuthentication GetNextAuthorizedClient(string sessionId)
        {
            IdentityManagerAuthentication nextClient = new IdentityManagerAuthentication();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Next_Authorized_Client"), connection);
                command.Parameters.Add("SESSION_ID", SqlDbType.VarChar, 500).Value = sessionId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        nextClient.RequestId = reader["REQ_ID"].ToString();

                        IdentityManagerClient client = new IdentityManagerClient();
                        client.Name = reader["CLIENT_NAME"].ToString();
                        client.ClientId = reader["CLIENT_ID"].ToString();
                        client.ClientSecret = reader["CLIENT_SECRET"].ToString();
                        client.HostName = reader["HOSTNAME"].ToString();
                        client.ClientEnabled = Convert.ToInt16(reader["CLIENT_ENABLED"].ToString());
                        client.SignoutUrl = reader["SIGNOUT_URL"].ToString();
                        client.LoginReplyParams = reader["LOGIN_REPLY_PARAMS"].ToString();
                        client.LoginReplyUrl = reader["LOGIN_REPLY_URL"].ToString();
                        client.RegisterReplyParams = reader["REGISTER_REPLY_PARAMS"].ToString();
                        client.RegisterReplyUrl = reader["REGISTER_REPLY_URL"].ToString();
                        client.SignoutReplyParams = reader["SIGNOUT_REPLY_PARAMS"].ToString();
                        client.SignoutReplyUrl = reader["SIGNOUT_REPLY_URL"].ToString();
                        nextClient.Client = client;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return nextClient;
        }

        public IdentityManagerClient GetClientInfo(string clientId)
        {
            IdentityManagerClient client = new IdentityManagerClient();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Client_Information_By_Client_Id"), connection);
                command.Parameters.Add("CLIENT_ID", SqlDbType.VarChar, 500).Value = clientId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        client.Name = reader["CLIENT_NAME"].ToString();
                        client.ClientId = reader["CLIENT_ID"].ToString();
                        client.ClientSecret = reader["CLIENT_SECRET"].ToString();
                        client.HostName = reader["HOSTNAME"].ToString();
                        client.ClientEnabled = Convert.ToInt16(reader["CLIENT_ENABLED"].ToString());
                        client.SignoutUrl = reader["SIGNOUT_URL"].ToString();

                        client.LoginReplyParams = reader["LOGIN_REPLY_PARAMS"].ToString();
                        client.LoginReplyUrl = reader["LOGIN_REPLY_URL"].ToString();
                        client.RegisterReplyParams = reader["REGISTER_REPLY_PARAMS"].ToString();
                        client.RegisterReplyUrl = reader["REGISTER_REPLY_URL"].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return client;
        }

        public ProfileImage GetProfileImageByKey(string imageKey)
        {
            ProfileImage client = new ProfileImage();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Profile_Image_By_Image_Key"), connection);
                command.Parameters.Add("IMAGE_KEY", SqlDbType.VarChar, 500).Value = imageKey;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        client.data = (byte[])reader["profileImage"];
                        client.mime = reader["profileImageMime"].ToString();
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return client;
        }

        public List<int> GetAvailableTimeSlotsDA() 
        {
            List<int> timeSlotIds = new List<int>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Available_Slots"), connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        var id = Convert.ToInt32(reader["Id"].ToString());
                        timeSlotIds.Add(id);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally 
                {
                    reader.Close();
                }
            }
            return timeSlotIds;
        }

        public bool SaveUserAuthentication(string clientId, string userId, string authenticationCode, string requestUrl, string sessionId)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Save_User_Auth_Request")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("REQ_ID", SqlDbType.VarChar, 100).Value = Guid.NewGuid().ToString();
                    querySaveStaff.Parameters.Add("CLIENT_ID", SqlDbType.VarChar, 100).Value = clientId;
                    querySaveStaff.Parameters.Add("SESSION_ID", SqlDbType.VarChar, 100).Value = sessionId;
                    querySaveStaff.Parameters.Add("USERID", SqlDbType.VarChar, 500).Value = userId;
                    querySaveStaff.Parameters.Add("AUTH_TOKEN", SqlDbType.VarChar, 100).Value = authenticationCode;
                    querySaveStaff.Parameters.Add("FULL_REQ_URL", SqlDbType.VarChar, 1000).Value = requestUrl;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }

        private bool SaveClientAuthentication(string requestId, string clientId, string authToken, string accessToken, string v)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Save_Client_Auth_Request")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("REQ_ID", SqlDbType.VarChar, 100).Value = requestId;
                    querySaveStaff.Parameters.Add("CLIENT_ID", SqlDbType.VarChar, 100).Value = clientId;
                    querySaveStaff.Parameters.Add("AUTH_TOKEN", SqlDbType.VarChar, 500).Value = authToken;
                    querySaveStaff.Parameters.Add("ACCESS_TOKEN", SqlDbType.VarChar, 100).Value = accessToken;
                    querySaveStaff.Parameters.Add("FULL_REQ_HEADER", SqlDbType.VarChar, 1000).Value = v;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }

        public List<Vacancy> GetAllVacancyDA(string keyword, string filter, bool IsRemote, int departmentId, int? clientId) 
        {

            List<Vacancy> vacancyList = new List<Vacancy>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_Vacancy").Replace("#KEYWORD#", keyword), connection);
                command.Parameters.Add("VACANCYTYPE", SqlDbType.VarChar, 100).Value = filter;
                command.Parameters.Add("ISREMOTE", SqlDbType.Bit, 100).Value = IsRemote;
                command.Parameters.Add("DEPARTMENTID", SqlDbType.Int, 100).Value = departmentId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        Vacancy vacancy = new Vacancy();
                        vacancy.Id = Convert.ToInt32(reader["Id"].ToString());
                        vacancy.Title = reader["Title"].ToString();
                        vacancy.CompanyName = reader["CompanyName"].ToString();
                        vacancy.Location = reader["Location"].ToString();
                        vacancy.Salary = reader["Salary"].ToString();
                        vacancy.SmallDescription = reader["SmallDescription"].ToString();
                        vacancy.VacancyStatus = Convert.ToInt32(reader["VacancyStatus"].ToString());

                        vacancyList.Add(vacancy);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                }
            }
            return vacancyList;
        }

        public Vacancy GetVacancyDA(int vacancyId) 
        {
            Vacancy vacancy = new Vacancy();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Vacancy"), connection);
                command.Parameters.Add("ID", SqlDbType.Int, 100).Value = vacancyId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    if (reader.Read())
                    {
                        vacancy.Id = Convert.ToInt32(reader["Id"].ToString());
                        vacancy.Title = reader["Title"].ToString();
                        vacancy.CompanyName = reader["CompanyName"].ToString();
                        vacancy.Location = reader["Location"].ToString();
                        vacancy.Salary = reader["Salary"].ToString();
                        vacancy.SmallDescription = reader["SmallDescription"].ToString();
                        vacancy.VacancyStatus = Convert.ToInt32(reader["VacancyStatus"].ToString());
                        vacancy.Duties = reader["Duties"].ToString();
                        vacancy.Qualifications = reader["Qualifications"].ToString();
                        vacancy.AdditionalInfo = reader["AdditionalInfo"].ToString();
                        vacancy.JobDescription = reader["JobDescription"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                }
            }
            return vacancy;
        }

        public DateTime GetInterviewRemainingTimeDA(int interviewId)
        {
            DateTime interviewTime = DateTime.Now;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Interview_Remaining_Time"), connection);
                command.Parameters.Add("ID", SqlDbType.Int, 100).Value = interviewId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    if (reader.Read())
                    {
                        interviewTime = Convert.ToDateTime(reader["InterviewTime"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                }
            }
            return interviewTime;
        }

        public void InsertApplicationDA(ApplicationFull applicationRequest) 
        {
            using (SqlConnection connection = GetConnection())
            {
                try
                {
                    SqlCommand command = new SqlCommand(GetQuery("Insert_Application"), connection);
                    command.Parameters.Add("FIRSTNAME", SqlDbType.VarChar, 100).Value = applicationRequest.Application.FirstName;
                    command.Parameters.Add("LASTNAME", SqlDbType.VarChar, 100).Value = applicationRequest.Application.LastName;
                    command.Parameters.Add("EMAIL", SqlDbType.VarChar, 100).Value = applicationRequest.Application.Email;
                    command.Parameters.Add("MOBILE", SqlDbType.VarChar, 100).Value = applicationRequest.Application.Mobile;
                    command.Parameters.Add("ADDRESS", SqlDbType.VarChar, 100).Value = applicationRequest.Application.Address;
                    command.Parameters.Add("CITY", SqlDbType.VarChar, 100).Value = applicationRequest.Application.City;
                    command.Parameters.Add("COUNTRY", SqlDbType.VarChar, 100).Value = applicationRequest.Application.Country;
                    command.Parameters.Add("LINKEDINID", SqlDbType.VarChar, 100).Value = applicationRequest.Application.LinkedInId;
                    command.Parameters.Add("FACEBOOKID", SqlDbType.VarChar, 100).Value = applicationRequest.Application.FacebookId;
                    command.Parameters.Add("GITHUBID", SqlDbType.VarChar, 100).Value = applicationRequest.Application.GithubId;
                    command.Parameters.Add("OTHERLINKID", SqlDbType.VarChar, 100).Value = applicationRequest.Application.OtherlinkId;
                    command.Parameters.Add("ISACKNOWLEDGED", SqlDbType.Bit, 100).Value = applicationRequest.Application.IsAcknowledged;
                    command.Parameters.Add("HMMESSAGE", SqlDbType.VarChar, 100).Value = applicationRequest.Application.HMMessage;
                    connection.Open();
                    int insertedId = (int)command.ExecuteScalar();

                    InsertQualificationDA(applicationRequest.Application.ApplicationQualifications, insertedId);
                    InsertExperieneceDA(applicationRequest.Application.ApplicationExperiences, insertedId);
                    InsertApplicationAnswersDA(applicationRequest.ApplicationAnswers, insertedId);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally 
                {
                    connection.Close();
                }
            }
        }

        public void InsertQualificationDA(IList<ApplicationQualification> qualifications, int? applicationId) 
        {
            if (qualifications.Count > 0)
            {
                using (SqlConnection connection = GetConnection())
                {
                    try
                    {
                        foreach (var item in qualifications)
                        {
                            SqlCommand command = new SqlCommand(GetQuery("Insert_Application_Qualification"), connection);
                            command.Parameters.Add("APPID", SqlDbType.Int, 100).Value = applicationId;
                            command.Parameters.Add("QUALIFICATIONNAME", SqlDbType.VarChar, 100).Value = item.QualificationName;
                            command.Parameters.Add("INSTITUTE", SqlDbType.VarChar, 100).Value = item.Institute;
                            command.Parameters.Add("DESCRIPTION", SqlDbType.Int, 100).Value = item.Description;
                            command.Parameters.Add("QUALIFICATIONYEAR", SqlDbType.DateTime, 100).Value = item.QualificationYear;
                            command.Parameters.Add("QUALIFICATIONTYPE", SqlDbType.DateTime, 100).Value = item.QualificationType;
                            command.Parameters.Add("ISCURRENT", SqlDbType.Bit, 100).Value = item.IsCurrent;
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally 
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertExperieneceDA(IList<ApplicationExperience> experiences, int? applicationId) 
        {
            if (experiences.Count > 0)
            {
                using (SqlConnection connection = GetConnection())
                {
                    try
                    {
                        foreach (var item in experiences)
                        {
                            SqlCommand command = new SqlCommand(GetQuery("Insert_Application_Experience"), connection);
                            command.Parameters.Add("APPID", SqlDbType.Int, 100).Value = applicationId;
                            command.Parameters.Add("EXPERIENCETITLE", SqlDbType.VarChar, 100).Value = item.ExperienceTitle;
                            command.Parameters.Add("COMPANY", SqlDbType.VarChar, 100).Value = item.Company;
                            command.Parameters.Add("DESCRIPTION", SqlDbType.Int, 100).Value = item.Description;
                            command.Parameters.Add("FROMDATE", SqlDbType.DateTime, 100).Value = item.FromDate;
                            command.Parameters.Add("TODATE", SqlDbType.DateTime, 100).Value = item.ToDate;
                            command.Parameters.Add("ISCURRENT", SqlDbType.Bit, 100).Value = item.IsCurrent;
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally 
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void InsertApplicationAnswersDA(IList<ApplicationAnswer> answers, int? applicationId) 
        {
            if (answers.Count > 0)
            {
                using (SqlConnection connection = GetConnection())
                {
                    try
                    {
                        foreach (var item in answers)
                        {
                            SqlCommand command = new SqlCommand(GetQuery("Insert_Application_Answer"), connection);
                            command.Parameters.Add("APPID", SqlDbType.Int, 100).Value = applicationId;
                            command.Parameters.Add("QUESTIONID", SqlDbType.VarChar, 100).Value = item.QuestionId;
                            command.Parameters.Add("ANSWER", SqlDbType.VarChar, 100).Value = item.Answer;
                            command.Parameters.Add("COMMENT", SqlDbType.Int, 100).Value = item.Comment;
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public async Task InsertAttachmentDA(Attachment attachment) 
        {
            using (SqlConnection connection = GetConnection())
            {
                try
                {
                    SqlCommand command = new SqlCommand(GetQuery("Insert_Attachment"), connection);
                    command.Parameters.Add("FILENAME", SqlDbType.Int, 100).Value = attachment.FileName;
                    command.Parameters.Add("FILEPATH", SqlDbType.Int, 100).Value = attachment.FilePath;
                    command.Parameters.Add("FILEEXTENSION", SqlDbType.Int, 100).Value = attachment.FileExtension;
                    command.Parameters.Add("FILEDESCRIPTION", SqlDbType.Int, 100).Value = attachment.FileDescription;
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public ApplicationInterview GetInterviewDA(int interviewId) 
        {
            ApplicationInterview interview = new ApplicationInterview();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Interview"), connection);
                command.Parameters.Add("ID", SqlDbType.Int, 100).Value = interviewId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    if (reader.Read())
                    {
                        interview.Id = Convert.ToInt32(reader["Id"].ToString());
                        interview.AppId = Convert.ToInt32(reader["AppId"].ToString());
                        interview.StepId = Convert.ToInt32(reader["StepId"].ToString());
                        interview.CalDate = Convert.ToDateTime(reader["CalDate"].ToString());
                        interview.CalTime = Convert.ToDateTime(reader["CalTime"].ToString());
                        interview.MeetingLink = reader["MeetingLink"].ToString();
                        //interview.IsInviteSent = Convert.ToBoolean(reader["IsInviteSent"].ToString());
                        interview.Status = Convert.ToInt32(reader["Status"].ToString());
                        //interview.InterviewMarks = Convert.ToDecimal(reader["InterviewMarks"].ToString());
                        interview.InterviewTime = Convert.ToDateTime(reader["InterviewTime"].ToString());
                        //interview.MeetingId = Guid.Parse(reader["MeetingId"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                }
            }
            return interview;
        }

        public bool UpdateInterviewDA(int interviewId, string meetingLink, Guid meetingId) 
        {
            using (SqlConnection connection = GetConnection())
            {
                try
                {
                    SqlCommand command = new SqlCommand(GetQuery("Update_Interview"), connection);
                    command.Parameters.Add("ID", SqlDbType.Int, 100).Value = interviewId;
                    command.Parameters.Add("MEETINGLINK", SqlDbType.VarChar, 100).Value = meetingLink;
                    command.Parameters.Add("MEETINGID", SqlDbType.UniqueIdentifier, 100).Value = meetingId;
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
                return true;
            }
        }

        public Application GetApplicationByIdDA(int applicationId)
        {
            Application application = new Application();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_Application"), connection);
                command.Parameters.Add("ID", SqlDbType.Int, 100).Value = applicationId;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    if (reader.Read())
                    {
                        application.VacancyId = Convert.ToInt32(reader["Id"].ToString());
                        application.IsAcknowledged = Convert.ToBoolean(reader["IsAcknowledged"].ToString());
                        application.Email = reader["Email"].ToString();
                        application.FirstName = reader["Firstname"].ToString();
                        application.LastName = reader["Lastname"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return application;
        }

    }
}
