﻿using Microsoft.Data.SqlClient;
using RecruitPlus.Core.DataAccess;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RecruitPlus.Core
{
    public partial class ScheduleService : BaseDataAccess
    {
        public bool IsHasAScheduledInterview(int slotId, int userId, DateTime scheduleDate)
        {           
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Has_A_Scheduled_Interview"), connection);
                command.Parameters.Add("UserId", SqlDbType.Int, 100).Value = userId;
                command.Parameters.Add("TimeSlotId", SqlDbType.Int, 100).Value = slotId;
                command.Parameters.Add("CalendarDate", SqlDbType.DateTime, 100).Value = scheduleDate;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return false;
        }

        public List<DailyTimeSlot> GetAllMarkedTimeslots(int userId, int year, int month)
        {
            List<DailyTimeSlot> output = new List<DailyTimeSlot>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand command = new SqlCommand(GetQuery("Get_All_Schedules"), connection);
                command.Parameters.Add("UserId", SqlDbType.Int, 100).Value = userId;
                command.Parameters.Add("appyear", SqlDbType.Int, 100).Value = year;
                command.Parameters.Add("appmonth", SqlDbType.Int, 100).Value = month;
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        DailyTimeSlot ts = new DailyTimeSlot();
                        ts.id = Convert.ToInt32(reader["Id"].ToString());
                        ts.name = reader["TimeSlotDescription"].ToString();
                        ts.start_time = Convert.ToDouble(reader["TimeStart"].ToString());
                        ts.end_time = Convert.ToDouble(reader["TimeEnd"].ToString());
                        ts.date = Convert.ToDateTime(reader["CalendarDate"].ToString());
                        output.Add(ts);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return output;
        }

        public bool DeleteAllEmployeeAvailabilitySlots(ScheduleRequestVM request)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Delete_All_Availability_By_Date")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("UserId", SqlDbType.Int, 100).Value = request.user_id;
                    querySaveStaff.Parameters.Add("CalendarDate", SqlDbType.DateTime, 100).Value = request.schedule_date;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }

        public bool SaveEmployeeAvailabilitySlot(int slotId, int userId, DateTime scheduleDate)
        {
            using (SqlConnection openCon = GetConnection())
            {
                using (SqlCommand querySaveStaff = new SqlCommand(GetQuery("Mark_Availability")))
                {
                    querySaveStaff.Connection = openCon;
                    querySaveStaff.Parameters.Add("UserId", SqlDbType.Int, 100).Value = userId;
                    querySaveStaff.Parameters.Add("TimeSlotId", SqlDbType.Int, 100).Value = slotId;
                    querySaveStaff.Parameters.Add("CalendarDate", SqlDbType.DateTime, 100).Value = scheduleDate;
                    openCon.Open();
                    querySaveStaff.ExecuteNonQuery();
                }
            }
            return true;
        }

    }
}
