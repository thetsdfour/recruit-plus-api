﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace RecruitPlus.Core.DataAccess
{
    public class BaseDataAccess
    {
        private string connectionString = String.Empty;
        private string xml = String.Empty;

        public BaseDataAccess(IConfiguration configuration, string xml)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            this.xml = xml;
        }


        public SqlConnection GetConnection()
        {
            return new SqlConnection(connectionString);
        }


        public string GetQuery(string attribute)
        {
            string fileContents = GetResourceTextFile(xml + ".xml");
            XmlDocument infodoc = new XmlDocument();
            infodoc.LoadXml(fileContents);

            var node = infodoc.SelectSingleNode("//*[@id='" + attribute + "']");

            return node.InnerText.Replace("<![CDATA[", "").Replace("]]>", "");
        }

        public string GetResourceTextFile(string filename)
        {
            string result = string.Empty;

            using (Stream stream = this.GetType().Assembly.GetManifestResourceStream("RecruitPlus.Core.Scripts." + filename))
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;
        }
    }
}
