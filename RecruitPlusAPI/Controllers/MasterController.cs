﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RecruitPlus.Core;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecruitPlusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        private readonly ILogger<MasterController> _logger;
        private IConfiguration _configuration;
        private MasterdataService masterdataService;

        public MasterController(ILogger<MasterController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            masterdataService = new MasterdataService(_configuration);
        }

        [Route("GetAllDepartments")]
        [HttpGet]
        public ApplicationResponse GetAllDepartments()
        {
            try
            {
                return new ApplicationResponse(masterdataService.GetAllDepartments());
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        [Route("Settings")]
        [HttpGet]
        public ApplicationResponse Settings()
        {
            try
            {
                return new ApplicationResponse(masterdataService.GetAppAppsettings());
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        [Route("Timeslots")]
        [HttpGet]
        public ApplicationResponse Timeslots()
        {
            try
            {
                return new ApplicationResponse(masterdataService.GetAllTimeSlots());
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }
    }
}
