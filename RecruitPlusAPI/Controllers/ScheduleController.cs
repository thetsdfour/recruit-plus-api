﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RecruitPlus.Core;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecruitPlusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly ILogger<ScheduleController> _logger;
        private IConfiguration _configuration;
        private ScheduleService scheduleService;

        public ScheduleController(ILogger<ScheduleController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            scheduleService = new ScheduleService(_configuration);
        }

        [Route("MarkEmployeeAvailability")]
        [HttpPost]
        public ApplicationResponse MarkEmployeeAvailability(ScheduleRequestVM request)
        {
            try
            {
                return (scheduleService.MarkEmployeeAvailability(request));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        [Route("GetEmployeeMarkedTimeSlots")]
        [HttpPost]
        public ApplicationResponse GetEmployeeMarkedTimeSlots(MonthlyScheduleRequest request)
        {
            try
            {
                return new ApplicationResponse(scheduleService.GetAllMarkedTimeslots(request.user_id,
                    request.year,request.month));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }



    }
}
