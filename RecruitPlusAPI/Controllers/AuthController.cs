﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RecruitPlus.Core;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecruitPlusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly ILogger<AuthController> _logger;
        private IConfiguration _configuration;
        private AuthenticationService authenticationService ;

        public AuthController(ILogger<AuthController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            authenticationService = new AuthenticationService(_configuration);
        }

        [Route("AuthenticateUser")]
        [HttpPost]
        public ApplicationResponse AuthenticateUser(AuthenticateRequest request)
        {
            try
            {
                return (authenticationService.AuthenicateUser(request));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

    }
}
