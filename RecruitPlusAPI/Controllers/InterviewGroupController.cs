﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RecruitPlus.Core;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecruitPlusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InterviewGroupController : ControllerBase
    {
        private readonly ILogger<InterviewGroupController> _logger;
        private IConfiguration _configuration;
        private HRPortalService hrPortalService;

        public InterviewGroupController(ILogger<InterviewGroupController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            hrPortalService = new HRPortalService(_configuration);
        }

        [Route("GetAll")]
        [HttpGet]
        public ApplicationResponse GetAllInterviewGroups()
        {
            try
            {
                return new ApplicationResponse(hrPortalService.GetAllInterviewGroups());
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message,false);
            }
        }

        [Route("Create")]
        [HttpPost]
        public ApplicationResponse CreateInterviewGroup(InterviewGroupCreateRequest request)
        {
            try
            {
                return new ApplicationResponse(hrPortalService.InsertInterviewGroup(request));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        [Route("GetMembers")]
        [HttpGet]
        public ApplicationResponse GetInterviewGroupMembersByInterviewGroupId(int groupId)
        {
            try
            {
                return new ApplicationResponse(hrPortalService.GetInterviewGroupMembersById(groupId));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        [Route("AddMember")]
        [HttpPost]
        public ApplicationResponse AddMemberToInterviewGroup(InterviewGroupRequest request)
        {
            try
            {
                return (hrPortalService.AddMemberToInterviewGroupById(request));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        [Route("RemoveMember")]
        [HttpPost]
        public ApplicationResponse RemoveMemberFromInterviewGroup(InterviewGroupRequest request)
        {
            try
            {
                return (hrPortalService.RemoveMemberFromInterviewGroupById(request));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

    }
}
