﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RecruitPlus.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecruitPlusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly ILogger<ApplicationController> _logger;
        private IConfiguration _configuration;
        private ApplicationService applicationService;

        public ApplicationController(ILogger<ApplicationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            applicationService = new ApplicationService(_configuration);
        }

    }
}
