﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RecruitPlus.Core;
using RecruitPlus.Core.Models;
using RecruitPlus.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RecruitPlusAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidatePortalController : ControllerBase
    {
        private IConfiguration _configuration;
        private CandidatePortalService _candidatePortalService;

        public CandidatePortalController(IConfiguration configuration)
        {
            _configuration = configuration;
            _candidatePortalService = new CandidatePortalService(_configuration);
        }

        //Get All Vacancy Details
        [Route("GetAllVacancy")]
        [HttpGet]
        public ApplicationResponse GetAllVacancy(string keyword, string filter, bool IsRemote, int departmentId, int? clientId) 
        {
            try
            {
                return new ApplicationResponse(_candidatePortalService.GetAllVacancy(keyword, filter, IsRemote, departmentId, clientId));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Get Vacancy Details
        [Route("GetVacancy")]
        [HttpGet]
        public ApplicationResponse GetVacancy(int vacancyId) 
        {
            try
            {
                return new ApplicationResponse(_candidatePortalService.GetVacancy(vacancyId));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Apply For Job Ad
        [Route("ApplyForVacancy")]
        [HttpPost]
        public ApplicationResponse ApplyForVacancy(ApplicationFull applicationRequest) 
        {
            try
            {
                var files = Request.Form.Files;
                var path = _configuration.GetSection("DocumentUploadPath").Value;

                _candidatePortalService.ApplyForVacancy(applicationRequest);

                foreach (var file in files)
                {
                    if (file != null && file.Length > 0)
                    {
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                    }

                    string fileName = Path.GetFileName(file.FileName);
                    using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    Attachment attachment = new Attachment();
                    attachment.FileName = fileName;
                    attachment.FilePath = Path.Combine(path, fileName);
                    attachment.FileExtension = fileName.Split('.')[1];

                    Task.Run(async () => await _candidatePortalService.SaveFile(attachment));
                }
                return new ApplicationResponse();
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Confirm Interview Participation
        [Route("ConfirmParticipation")]
        [HttpPost]
        public ApplicationResponse ConfirmParticipation(List<int> availableTimeSlots) 
        {
            try
            {
                return new ApplicationResponse();
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Get Available Time Slots to Schedule Interview
        [Route("GetAvailableTimeSlots")]
        [HttpGet]
        public ApplicationResponse GetAvailableTimeSlots() 
        {
            try
            {
                return new ApplicationResponse(_candidatePortalService.GetAvailableTimeSlots());
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Get Remaining Time for Interview
        [Route("GetInterviewRemainingTime")]
        [HttpGet]
        public ApplicationResponse GetInterviewRemainingTime(int interviewId) 
        {
            try
            {
                return new ApplicationResponse(_candidatePortalService.GetInterviewRemainingTime(interviewId));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Create Interview Link
        [Route("CreateInterviewLink")]
        [HttpPost]
        public ApplicationResponse CreateInterviewLink(int interviewId) 
        {
            try
            {
                return new ApplicationResponse(_candidatePortalService.CreateInterviewLink(interviewId));
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Get Interview Link
        [Route("GetInterviewLink")]
        [HttpGet]
        public ApplicationResponse GetInterviewLink(int interviewId) 
        {
            try
            {
                
                return new ApplicationResponse();
            }
            catch (Exception ex)
            {
                return new ApplicationResponse(ex.Message, false);
            }
        }

        //Get Company Details

    }
}
